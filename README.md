# LMDE Expert Mode Installation Helper Script

Used to provide good defaults to LMDE to install with or without encryption, hibernation support, and with BtrFS subvolumes.

```
Usage: lmde-expert.sh <root> <boot> <efi> [<options>...]

Positional Arguments:
<root>    Root Partition used for LUKS or BtrFS filesystem
<boot>    Boot Partition or @boot for subvolume in BtrFS
<efi>     ESP Partition for booting

Options:
-h, --help        Help on this tool.
-e, --encryption  Enable LUKS encryption.
-s, --swap        Enable Swap/Hibernation support.
-d, --debug       Enable DEBUG mode for testing.
```

## Initial Setup

To prepare your disk for this, you need to pre-create the paritions needed as you have to supply those partitions to the installer.

Do this using cfdisk or GParted from the live media of the LMDE environment.

You will need:

* 100MB for /boot/efi as FAT32 with boot+esp flags.
* 2GB for /boot if you're using encryption, otherwise you can supply @boot to the installer.
* Remainder for / as BtrFS

## Installation

Run lmde-expert.sh with appropriate parameters. An example of this for NVME-based SSD, and Encryption with Hibernation:

```
sudo ./lmde-expert.sh /dev/nvme0n1p3 /dev/nvme0n1p2 /dev/nvme0n1p1 -e -s
```

This will start the provisioning setup, with the full expectations that the system is ready to go as-is, as it will format. You will get one prompt before it actually starts.

## Expert-Step

After the installation gets to the step where you run the "expert" portion, you can run this again with the --expert flag, and it will finish the installation steps during this phase.

## Post-Installation

After you've installed and booted for the first time, to get BtrFS Snapshots setup, you can run the postinstall script:

```
sudo ./lmde-postinstall.sh
```

This will simply install and setup btrfs with snapshots and create your first snapshot automatically.
