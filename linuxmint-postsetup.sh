#!/bin/bash

if [[ "$(id -u)" -ne 0 ]]; then
	echo "ERROR: This needs to be run as root"
	exit 250
fi

declare -rA SUBVOLS_DEFAULT=(
  ["@root"]="root"
  ["@srv"]="srv"
  ["@opt"]="opt"
  ["@local"]="usr/local"
  ["@cache"]="var/cache"
  ["@containers"]="var/lib/containers"
  ["@libvirt"]="var/lib/libvirt/images"
  ["@machines"]="var/lib/machines"
  ["@portables"]="var/lib/portables"
  ["@log"]="var/log"
  ["@spool"]="var/spool"
  ["@tmp"]="var/tmp"
  ["@www"]="var/www"
  ["@snapshots"]=".snapshots"
)

function show_help() {
	echo "Usage: $0 <root> <boot> <efi> [<options>...]"
	echo ""
	echo "Positional Arguments:"
	echo "<root>    Root Partition used for LUKS or BtrFS filesystem"
	echo "<boot>    Boot Partition or @boot for subvolume in BtrFS"
	echo "<efi>     ESP Partition for booting"
	echo ""
	echo "Options:"
	echo "-h, --help        Help on this tool."
    echo "-e, --encryption  Enable LUKS encryption."
	echo "-s, --swap        Enable Swap/Hibernation support."
	echo "-d, --debug       Enable DEBUG mode for testing."
	exit 0
}

function create_subvolumes() {
	local -a subvols

	if [[ "$DEBUG" ]]; then
		local cmd="echo"
	else
		local cmd=""
	fi

	subvols+=("${!SUBVOLS_DEFAULT[@]}")

	if [[ "$ENCRYPTION" ]]; then
		${cmd} mount -o noatime,space_cache=v2,ssd "/dev/mapper/$ENC_VOL" /mnt
	else
		${cmd} mount -o noatime,space_cache=v2,ssd "$RootPart" /mnt
	fi
    if [[ "$?" -ne 0 ]]; then
        echo "FATAL: Could not mount /mnt"
        exit 100
    fi

	if [[ "$BootPart" == "@boot" ]]; then
        ${cmd} btrfs subvolume create /mnt/@boot
        ${cmd} rsync -avhHi --delete-after "/mnt/@/boot/" "/mnt/@boot/"
	fi

	if [[ "$SWAP" ]]; then
		${cmd} btrfs subvolume create /mnt/@swap
	fi

	for subvol in "${subvols[@]}"
	do
		${cmd} btrfs subvolume create /mnt/"$subvol"
        if [[ -d "/mnt/@/${SUBVOLS_DEFAULT[$subvol]}" || "$DEBUG" ]]; then
            ${cmd} rsync -avhHi --delete-after "/mnt/@/${SUBVOLS_DEFAULT[$subvol]}/" "/mnt/$subvol/"
        fi
	done

	${cmd} umount /mnt
}

function unmount_target() {
    local subvol

	if [[ "$DEBUG" ]]; then
		local cmd="echo"
	else
		local cmd=""
	fi

    for subvol in "${!SUBVOLS_DEFAULT[@]}"
    do
        ${cmd} umount /target/"${SUBVOLS_DEFAULT[$subvol]}"
    done

    if [[ "$SWAP" ]]; then
        ${cmd} umount /target/swap
    fi

    ${cmd} umount /target/boot/efi
    ${cmd} umount /target/boot
    ${cmd} umount /target

    if [[ "$ENCRYPTION" ]]; then
        ${cmd} cryptsetup luksClose "$ENC_VOL"
    fi
}

function get_hibernate_size() {
	free --giga | awk '/^Mem:/{print $2}'
}

function prepare_target() {
	local subvol
	local rootmount

	if [[ "$DEBUG" ]]; then
		local cmd="echo"
	else
		local cmd=""
	fi

	${cmd} mkdir /target

	rootmount="$RootPart"
	if [[ "$ENCRYPTION" ]]; then
        rootmount="/dev/mapper/$ENC_VOL"
	fi

	${cmd} mount -o noatime,space_cache=v2,ssd,subvol=@ "$rootmount" /target
    if [[ "$?" -ne 0 ]]; then
        echo "FATAL: Could not mount /target"
        exit 101
    fi

	for subvol in "${!SUBVOLS_DEFAULT[@]}"
	do
        if [[ ! -d "/target/${SUBVOLS_DEFAULT[$subvol]}" || "$DEBUG" ]]; then
		    ${cmd} mkdir -p /target/"${SUBVOLS_DEFAULT[$subvol]}"
        fi
	done

	#${cmd} mkdir -p /target/boot
	if [[ "$BootPart" == "@boot" ]]; then
		${cmd} mount -o noatime,space_cache=v2,ssd,subvol=@boot "$rootmount" /target/boot
	else
		${cmd} mount "$BootPart" /target/boot
	fi
    if [[ "$?" -ne 0 ]]; then
        echo "FATAL: Could not mount /target/boot"
        exit 102
    fi

    ${cmd} mount -o noatime,space_cache=v2,ssd,subvol=@home "$rootmount" /target/home
    if [[ "$?" -ne 0 ]]; then
        echo "FATAL: Could not mount /target/home"
        exit 103
    fi

	for subvol in "${!SUBVOLS_DEFAULT[@]}"
	do
		${cmd} mount -o noatime,space_cache=v2,ssd,subvol="$subvol" "$rootmount" /target/"${SUBVOLS_DEFAULT[$subvol]}"
	done

    if [[ ! -d "/target/boot/efi" ]]; then
	    ${cmd} mkdir -p /target/boot/efi
    fi
	${cmd} mount "$EFIPart" /target/boot/efi
    if [[ "$?" -ne 0 ]]; then
        echo "FATAL: Could not mount /target/boot/efi"
        exit 104
    fi

	if [[ "$SWAP" ]]; then
        if [[ ! -d "/target/swap" ]]; then
		    ${cmd} mkdir -p /target/swap
        fi
		${cmd} mount -o noatime,ssd,subvol=@swap "$rootmount" /target/swap
        if [[ "$?" -ne 0 ]]; then
            echo "FATAL: Could not mount /target/swap"
            exit 105
        fi
		${cmd} btrfs filesystem mkswapfile --size "$(get_hibernate_size)g" --uuid clear /target/swap/hibernate.swp
	fi

    if [[ "$SWAP" || "$BootPart" == "@boot" ]]; then
        ${cmd} arch-chroot /target update-grub2
    fi

	${cmd} apt install -y arch-install-scripts

	if [[ "$DEBUG" ]]; then
		echo "genfstab -U /target > /target/etc/fstab"
        echo
	else
		genfstab -U /target > /target/etc/fstab
	fi

	if [[ "$SWAP" ]]; then
		if [[ "$DEBUG" ]]; then
			echo "echo \"/swap/hibernate.swp none swap defaults 0 0\" >> /target/etc/fstab"
		else
			echo "/swap/hibernate.swp none swap defaults 0 0" >> /target/etc/fstab
		fi
		SwapUUID=$(grep btrfs /target/etc/fstab | head -n1 | cut -f1)
		SwapOffset=$(btrfs inspect-internal map-swapfile -r /target/swap/hibernate.swp)
		${cmd} sed -i "/^GRUB_CMDLINE_LINUX_DEFAULT=/ s/\(\"[^\"]*\)$/ resume=${SwapUUID} resume_offset=${SwapOffset}&/" /target/etc/default/grub
	fi

    #if [[ "$ENCRYPTION" ]]; then
    #
    #fi
}

function expert_step() {
	local UUID PART_ENTRY_UUID
	local SwapUUID SwapOffset

	if [[ "$DEBUG" ]]; then
		local cmd="echo"
	else
		local cmd=""
	fi

	${cmd} apt install -y arch-install-scripts

	if [[ "$DEBUG" ]]; then
		echo "genfstab -U /target > /target/etc/fstab"
        echo "==== genfstab result : begin ===="
        genfstab -U /target
        echo "==== genfstab result : end ===="
        echo
	else
		genfstab -U /target > /target/etc/fstab
	fi

	if [[ "$SWAP" ]]; then
		if [[ "$DEBUG" ]]; then
			echo "echo \"/swap/hibernate.swp none swap defaults 0 0\" >> /target/etc/fstab"
		else
			echo "/swap/hibernate.swp none swap defaults 0 0" >> /target/etc/fstab
		fi
		SwapUUID=$(grep btrfs /target/etc/fstab | head -n1 | cut -f1)
		SwapOffset=$(btrfs inspect-internal map-swapfile -r /target/swap/hibernate.swp)
		${cmd} sed -i "/^GRUB_CMDLINE_LINUX_DEFAULT=/ s/\(\"[^\"]*\)$/ resume=${SwapUUID} resume_offset=${SwapOffset}&/" /target/etc/default/grub
	fi
}

function show_options() {
	echo "Root Partition: $RootPart"
	echo "Boot Partition: $BootPart"
	echo "EFI Partition:  $EFIPart"

	if [[ "$ENCRYPTION" ]]; then
		echo "Encryption:     Enabled"
        echo "+ Volume:       $ENC_VOL"
	else
		echo "Encryption:     Disabled"
	fi

	if [[ "$SWAP" ]]; then
		echo "Swap:           Enabled"
	else
		echo "Swap:           Disabled"
	fi

    if [[ "$DEBUG" ]]; then
        echo "Debug:          Enabled"
    else
		echo "Debug:          Disabled"
	fi
	echo
}

function install_normal() {
	show_options

	read -rsn1 -p"Pre-Installation: To proceed, press enter to continue." proceed
	echo
	if [[ "$proceed" != "" ]]; then
		echo "Aborting."
		exit 1
	fi

	echo
	echo "Preparing Subvolumes..."
	create_subvolumes

	echo
	echo "Preparing Installation Target..."
	prepare_target

	echo
	echo "Post-Setup Preparations Complete. You can verify things in /target or you can"
    echo "re-run this script with --unmount to unmount and reboot."
}

function install_unmount() {
	show_options

	read -rsn1 -p"Post-Install Unmount and Reboot: To proceed, press enter to continue." proceed
	echo

	if [[ "$proceed" != "" ]]; then
		echo "Aborting."
		exit 1
	fi

	echo
	echo "Running Expert-Mode Installation Steps..."
	unmount_target
    reboot
}

function install_cleanup() {
	if [[ "$DEBUG" ]]; then
		local cmd="echo"
	else
		local cmd=""
	fi

	for subvol in "${!SUBVOLS_DEFAULT[@]}"
	do
		${cmd} umount /target/"${SUBVOLS_DEFAULT[$subvol]}"
	done

	if [[ "$SWAP" ]]; then
		${cmd} umount /target/swap
	fi

	${cmd} umount /target/boot/efi
	${cmd} umount /target/boot
	${cmd} umount /target

	if [[ "$ENCRYPTION" ]]; then
		${cmd} cryptsetup luksClose "$ENC_VOL"
	fi

	${cmd} dd if=/dev/zero of="${RootPart}" bs=1024 count=10
	${cmd} dd if=/dev/zero of="${EFIPart}" bs=1024 count=10
	if [[ "$BootPart" != "@boot" ]]; then
		${cmd} dd if=/dev/zero of="${BootPart}" bs=1024 count=10
	fi
}


declare -a POSITIONAL_ARGS=()
declare INSTALL_MODE="normal"

while [[ $# -gt 0 ]]; do
	case $1 in
		-h|--help)
			show_help
			;;
        -e|--encryption)
            ENCRYPTION=true
            ENC_VOL="$2"
            shift
            ;;
		-s|--swap)
			SWAP=true
			shift
			;;
		-d|--debug)
			DEBUG=true
			shift
			;;
		--unmount)
			INSTALL_MODE=unmount
			shift
			;;
		--clean)
			INSTALL_MODE=clean
			shift
			;;
		*)
			POSITIONAL_ARGS+=("$1") # save positional arg
			shift
			;;
	esac
done

set -- "${POSITIONAL_ARGS[@]}" # restore positional parameters

RootPart="$1"
BootPart="$2"
EFIPart="$3"

if [[ -z "$RootPart" || -z "$BootPart" || -z "$EFIPart" ]]; then
	echo "ERROR: Invalid parameters. See --help for help"
	exit 3
else
	if [[ "$ENCRYPTION" && "$BootPart" == "@boot" ]]; then
        if [[ -z "$ENC_VOL" ]]; then
            echo "Encryption volume needs to be set. Reliably determining this is"
            echo "best done by human eyes. To find your current volume, you can"
            echo "run:"
            echo "  dmsetup ls --target crypt"
            echo "If it's not active for whatever reason, you can open it with:"
            echo "  cryptsetup luksOpen "$RootPart" <volume_name>"
            exit 4
        elif [[ "$BootPart" == "@boot" ]]; then
        	echo "While encryption is enabled, using @boot subvolume will cause issues."
		    echo "Please prepare and set boot volume for /boot while using encryption."
		    exit 5
        fi
	fi

	case "$INSTALL_MODE" in
		normal)     install_normal;;
		unmount)	install_unmount;;
		clean)      install_cleanup;;
		*)
			echo "Error, unknown installation mode detected."
			exit 3
			;;
	esac
fi
