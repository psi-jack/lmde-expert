#!/bin/bash

if [[ "$(id -u)" -ne 0 ]]; then
	echo "ERROR: This needs to be run as root"
	exit 250
fi

apt install -y snapper inotify-tools git make systemd-zram-generator btrfsmaintenance axel extrepo
bash -c "$(curl -fsSL https://pacstall.dev/q/install)"

pushd /

umount /.snapshots
rm -r .snapshots
snapper -c root create-config /
btrfs subvolume delete /.snapshots
mkdir /.snapshots
mount -av

systemctl disable snapper-boot.timer
snapper -c root set-config 'TIMELINE_CREATE=no'
snapper -c root set-config 'ALLOW_GROUPS=sudo'
snapper -c root set-config 'SYNC_ACL=yes'
snapper -c root set-config 'NUMBER_LIMIT=10'
snapper -c root set-config 'NUMBER_LIMIT_IMPORTANT=10'
snapper -c root set-config 'TIMELINE_MIN_AGE=1800'
snapper -c root set-config 'TIMELINE_LIMIT_HOURLY=5'
snapper -c root set-config 'TIMELINE_LIMIT_DAILY=7'
snapper -c root set-config 'TIMELINE_LIMIT_WEEKLY=1'
snapper -c root set-config 'TIMELINE_LIMIT_MONTHLY=0'
snapper -c root set-config 'TIMELINE_LIMIT_YEARLY=0'

snapper -c root create --description "Fresh install"

cd /tmp || exit 240
git clone https://github.com/Antynea/grub-btrfs.git
cd grub-btrfs || exit 240
make install
systemctl enable --now grub-btrfsd
update-grub2
cd /tmp || exit 240
rm -rf grub-btrfs

echo "zram-size = 2048" >> /etc/systemd/zram-generator.conf
echo "compression-algorithm = zstd" >> /etc/systemd/zram-generator.conf

systemctl enable --now btrfs-{scrub,balance,trim}.timer

popd || exit 240
